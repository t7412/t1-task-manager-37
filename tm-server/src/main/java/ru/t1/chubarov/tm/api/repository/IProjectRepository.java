package ru.t1.chubarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {

    Project update(@NotNull Project project) throws Exception;

}
