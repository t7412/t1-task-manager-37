package ru.t1.chubarov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.service.IAuthService;
import ru.t1.chubarov.tm.api.service.IPropertyService;
import ru.t1.chubarov.tm.api.service.ISessionService;
import ru.t1.chubarov.tm.api.service.IUserService;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.exception.user.AccessDeniedException;
import ru.t1.chubarov.tm.exception.user.AuthenticationException;
import ru.t1.chubarov.tm.model.Session;
import ru.t1.chubarov.tm.model.User;
import ru.t1.chubarov.tm.util.CryptUtil;
import ru.t1.chubarov.tm.util.HashUtil;

import java.util.Date;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService,
            @NotNull final ISessionService sessionService) {
        this.propertyService = propertyService;
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Override
    @NotNull
    public User registry(@NotNull final String login, @NotNull final String password, @NotNull final String email) throws Exception {
        return userService.create(login, password, email);
    }

    @Override
    public void logout(@Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException();
        sessionService.findOneById(session.getId());
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final boolean locked = user.getLocked() == null || user.getLocked();
        if (locked) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return getToken(user);
    }

    @NotNull
    private String getToken(@NotNull final User user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final Session session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey(token);
        return CryptUtil.encrypt(sessionKey, token);
    }

    @SneakyThrows
    @NotNull
    private Session createSession(@NotNull final User user) {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.add(session);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session validateToken(@Nullable final String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(json, Session.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        @NotNull final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();

    //    if (sessionService.existsById(session.getId())) throw new AccessDeniedException();
        return session;
    }

    @SneakyThrows
    @Override
    public void invalidate(@NotNull final Session session) {
        if (session == null) return;
        sessionService.remove(session);
    }

}
