package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull Role role) throws Exception;

    boolean isLoginExist(@Nullable String login) throws Exception;

    boolean isEmailExist(@Nullable String email) throws Exception;

    @Nullable
    User findByLogin(@NotNull String login) throws Exception;

    @Nullable
    User findByEmail(@NotNull String email) throws Exception;

    @Nullable
    User removeOne(@NotNull User model) throws Exception;

    @Nullable
    User removeByLogin(@NotNull String login) throws Exception;

    @NotNull
    User removeByEmail(@NotNull String email) throws Exception;

    @Nullable
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @Nullable
    User updateUser(@Nullable String id,
                    @Nullable String firstName,
                    @Nullable String lastName,
                    @Nullable String middleName) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;
}
