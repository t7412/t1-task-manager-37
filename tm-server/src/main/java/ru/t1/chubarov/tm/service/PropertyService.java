package ru.t1.chubarov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.service.IPropertyService;

import java.io.IOException;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";
    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";
    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";
    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";
    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "25452";
    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";
    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "3565958575";
    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";
    @NotNull
    public static final String EMPTY_VALUE = "---";
    @NotNull
    public static final String SERVER_PORT = "server.port";
    @NotNull
    public static final String SERVER_HOST = "server.host";
    @NotNull
    public static final String SERVER_HOST_DEFAULT = "0.0.0.0";
    @NotNull
    public static final String SERVER_PORT_DEFAULT = "6262";
    @NotNull
    public static final String SESSION_KEY = "session.key";
    @NotNull
    public static final String SESSION_TIMEOUT = "session.timeout";
    @NotNull
    public static final String DATABASE_USERNAME_PROPERTY = "database.username";
    @NotNull
    public static final String DATABASE_USERNAME_JAVA_OPTION = "DATABASE_USERNAME";
    @NotNull
    public static final String DATABASE_USERNAME_DEFAULT = "postgres";
    @NotNull
    public static final String DATABASE_PASSWORD_PROPERTY = "database.password";
    @NotNull
    public static final String DATABASE_PASSWORD_JAVA_OPTION = "DATABASE_PASSWORD";
    @NotNull
    public static final String DATABASE_PASSWORD_DEFAULT = "admin";
    @NotNull
    public static final String DATABASE_URL_PROPERTY = "database.url";
    @NotNull
    public static final String DATABASE_URL_JAVA_OPTION = "DATABASE_URL";
    @NotNull
    public static final String DATABASE_URL_DEFAULT = "jdbc:postgressql://127.0.0.1:5432/TASKMANAGER";
    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        try {
            properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
        } catch (@NotNull final IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @Override
    @NotNull
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @Override
    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    public Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT, SERVER_PORT_DEFAULT);
    }

    @Override
    @NotNull
    public String getServerPortStr() {
        return getStringValue(SERVER_PORT, SERVER_PORT_DEFAULT);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getSessionKey(@NotNull final String token) {
        return getStringValue(SESSION_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT, "0");
    }

    @NotNull
    @Override
    public String getDatabaseUsername() {
        return getStringValue(DATABASE_USERNAME_PROPERTY, DATABASE_USERNAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD_PROPERTY, DATABASE_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DATABASE_URL_PROPERTY, DATABASE_URL_DEFAULT);
    }

}
