package ru.t1.chubarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.DBConstants;
import ru.t1.chubarov.tm.api.repository.ISessionRepository;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.model.Session;

import java.sql.*;

public final class SessionRepository extends AbstractUserOwnerRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstants.TABLE_SESSION;
    }

    @NotNull
    @Override
    public Session fetch(@NotNull ResultSet row) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(row.getString(DBConstants.COLUMN_ID));
        session.setDate(row.getTimestamp(DBConstants.COLUMN_DATE));
        session.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        session.setRole(Role.valueOf(row.getString(DBConstants.COLUMN_ROLE)));
        return session;
    }

    @NotNull
    @Override
    public Session add(@NotNull final Session session) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s) VALUES (?,?,?,?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_DATE,
                DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            if (session.getRole() == null) session.setRole(Role.USUAL);
            statement.setString(1, session.getId());
            statement.setTimestamp(2, new Timestamp(session.getDate().getTime()));
            statement.setString(3, session.getUserId());
            statement.setString(4, session.getRole().toString());
            statement.executeUpdate();
        }
        return session;
    }

    @NotNull
    public Session add(@NotNull final String userId, @NotNull final Session session) throws Exception {
        session.setUserId(userId);
        return add(session);
    }

    public void update(@NotNull final Session session) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?)",
                getTableName(), DBConstants.COLUMN_DATE,
                DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(session.getDate().getTime()));
            statement.setString(2, session.getUserId());
            statement.setString(3, session.getRole().toString());
            statement.executeUpdate();
        }
    }

}
