package ru.t1.chubarov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractUserOwnerModel extends AbstractModel {

    @Nullable
    private String userId;

//    @Nullable
//    public String getUserId() {
//        return userId;
//    }
//
//    public void setUserId(@Nullable String userId) {
//        this.userId = userId;
//    }

}
