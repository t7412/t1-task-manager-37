package ru.t1.chubarov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateResponse extends AbstractTaskResponse {

    @Nullable
    private Task task;

    public TaskCreateResponse(@Nullable final Task task) {
        this.task = task;
    }

}
